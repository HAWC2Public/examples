#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar  3 13:08:35 2022

@author: dave
"""

import os

import numpy as np
# import matplotlib as mpl
from matplotlib import pyplot as plt
from wetb.prepost import windIO
from wetb.hawc2 import HTCFile
plt.rc('text', usetex=True)
plt.rc('font', family='serif')

def check(fname):

    print(fname)
    f1 = os.path.dirname(fname)
    f2 = os.path.basename(fname)
    res = windIO.LoadResults(f1, f2)
    df = res.sig2df()

    # for k in sorted(df.columns): print(k)

    zs = np.array([1, 10, 20, 25, 40, 50, 60, 70, 80])
    velx, vely, velz = [], [], []
    for z in zs:
        velx.append(df[f'W_vel_x-0.00-0.00-{z:1.02f}'].mean())
        vely.append(df[f'W_vel_y-0.00-0.00-{z:1.02f}'].mean())
        velz.append(df[f'W_vel_z-0.00-0.00-{z:1.02f}'].mean())


    # expected current profile
    htc = HTCFile(filename='test_hydro_current.htc')
    t1, t2, t3, t4 = htc['hydro']['water_properties']['current'].values
    mudlevel = htc['hydro']['water_properties']['mudlevel'].values[0]
    mwl = htc['hydro']['water_properties']['mwl'].values[0]
    if t1 == 2:
        U0 = t2
        alpha = t3
        direction = t4

    velx_exp = U0 * ((mudlevel - mwl - zs) / (mudlevel - mwl)) ** alpha

    plt.figure()
    plt.plot(velx, -zs, label='HAWC2')
    plt.plot(velx_exp, -zs, label='expected')
    plt.legend(loc='best')
    plt.grid()
    plt.xlabel('velocity [m/s]')
    plt.ylabel('depth (local hydro) [m]')
    plt.savefig('test_hydro_current.png')

check('res/test_hydro_current')
fname = 'res/test_hydro_current'

