# Hydrodynamic current

## Introduction

This example contain an htc file of two beams exposed to hydrodynamic current in HAWC2. This example can be run with HAWC2 for any available platform.

## Modelling

The hydrodynamic current is modelled using the Morisson equation.

## Specifying the water properties

The water properties are defined in the `water_properties` block under the `hydro` block. The density of the water and the gravitational acceleration are specified along with the current. The `type` specifies the current profile, the `vel` gives the current velocity at the mean water level (`mwl`), the current parameter `alpha` is the profile parameter when `type` is 2, and `dir` is the current direction relative to `wave_direction`, clockwise positive.

```
begin water_properties;
    rho 1025 ; kg/m^3
    gravity 9.81 ; m/s^2
    mwl 0.0 ;
    mudlevel 100 ;
    wave_direction 0.0;
    ; Current direction relative to wave direction [deg]. Positive direction if current comes from the right looking towards the incoming waves.
    ;        type   vel  alpha   dir
    current    2    2.0    2.0  -90;

end water_properties;  
```

The power-law current profiles for varying values of $\alpha$ is shown in the plot below:

![Parametric study of the effect of $\alpha$ in the power law current profile.](hydro-current-power-law-profiles.png)

## Specifying the hydrodynamic parameters of the HAWC2 `hydro_element`

HAWC2 applies the hydrodynamic forces onto the model's `hydro_element`s. A `hydro_element` applies hydrodynamic parameters to an existing `main_body` in the HAWC2 model, and the elements are defined in a `hydro_element` block under the `hydro` block. An example of such a definition is shown below. The details of the available/required parameters can be found in the HAWC2 manual "How2HAWC2.pdf" in the chapter on hydrodynamics.

```
begin hydro_element;
    mbdy_name beam_2 ;
    update_states 1;
    buoyancy 0;
    hydrosections auto 40 ; distribution of hydro calculation points from sec 1 to nsec
    nsec 2;
;                                                                                        skin friction   magnus coefficient
;           z   Ca    Cd     A   Aref  width dr/dz  Cd_a_(quad)    Ca_a   Cd_a_lin    Aif          Cfmz         Cgam
     sec  0.0  2.0   1.5  23.7   23.7   5.5    0.0    0.0           0.0       0.0     0.0           0.0          1.0;
     sec 80.0  2.0   1.5  23.7   23.7   5.5    0.0    0.0           0.0       0.0     0.0           0.0          1.0;
end hydro_element;
```

## Running the example

The example can be run using the following command line:

```cmd
HAWC2MB.exe test_hydro_current.htc
```

The water current profile produced in HAWC2 can be verified with the python script `verify.py` which creates an illustration similar to the one below. Note that the script requires `numpy`, `matplotlib` and `wetb` to run properly.

![Expected and realized water current profile.](test_hydro_current.png)
