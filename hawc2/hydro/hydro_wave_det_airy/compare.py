#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 14 14:28:53 2023

@author: dave
"""

import numpy as np
from matplotlib import pyplot as plt

from wetb.prepost import windIO


def make_det_airy_waves():
    """Generate HAWC2 file used in det_airy (wavetype=2)
    """

    time = np.arange(0, 200.05, 0.05)
    wave_elevation = np.sin(time*0.10)
    # plt.plot(time, wave_elevation)
    data = np.array((time, wave_elevation)).T
    np.savetxt('wave_elevation.dat', data, fmt='% 7.04f', delimiter='    ',
               header='Time [s]  Wave elevation [m]')


def compare_input_output():
    """Compare the input wave field with the HAWC2 output
    """

    res = windIO.LoadResults('', 'hydro_waves.hdf5')
    df = res.sig2df()
    wavel = np.loadtxt('wave_elevation.dat')

    plt.figure()
    plt.plot(df['Time'], df['watersurface-global-0.00-0.00'], 'r-', label='H2')
    plt.plot(wavel[:,0], -wavel[:,1], 'k--', label='airy_det input')
    plt.xlim([0, 2])
    plt.legend(loc='best')
    plt.xlabel('time [s]')
    plt.grid()
    plt.ylabel('wave elevation (global coordinates')
    plt.savefig('det_airy_start.png')

    plt.figure()
    plt.plot(df['Time'], df['watersurface-global-0.00-0.00'], 'r-', label='H2')
    plt.plot(wavel[:,0], -wavel[:,1], 'k--', label='airy_det input')
    plt.xlim([198, 200])
    plt.legend(loc='best')
    plt.xlabel('time [s]')
    plt.grid()
    plt.ylabel('wave elevation [m] (global coord.)')
    plt.savefig('det_airy_end.png')

    plt.figure()
    plt.plot(df['Time'], df['watersurface-global-0.00-0.00'], 'r-', label='H2')
    plt.plot(wavel[:,0], -wavel[:,1], 'k--', label='airy_det input')
    plt.legend(loc='upper left')
    plt.xlabel('time [s]')
    plt.grid()
    plt.ylabel('wave elevation (global coordinates')
    plt.savefig('det_airy_full.png')
