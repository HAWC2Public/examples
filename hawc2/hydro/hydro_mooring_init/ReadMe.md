# Initialization of mooring Lines

## Introduction 

In this example, we add some mooring lines to a simple example with a floating column, ballasted with some point mass at the end.

## Initialization simulation

The mooring lines are initially positioned on the bed floor, straight.
As en example, the definition of the first mooring line is:

```
begin ext_sys;
	module							ElasticBar;
	name							line1;
	dll								./ESYSMooring.dll;
	ndata							17; 						Number of data input lines below
	data	nelem					51; 						Number of elements in line
	data	mass					45        40;			mass per length in air and in water [kg/m] 90% of original value
	data	start_pos				0.	120. 	193.49;		X-Y-Z-coordinate for first node (global coordinates) [m]
	data	end_pos					0	-650.0	193.5;			X-Y-Z-coordinate for last node (global coordinates) [m]
	data	cdp_cdl_cm				103.32	0.0		11.361;		Drag and added mass coefficients [special dimensions]
	data	axial_stiff				3.3E+09;					Line axial stiffness [N]
	data	read_write_initcond		0				1;			Read or write initial line geometry from file
	data    read_write_initcond_file   ./lineinitcond.dat;      File name for the initial line geometry
	data 	bottom_prop				193.5	0.05	1;			Seabed properties
	data	damping_ratio			0.01;						Structural damping ratio
	data	apply_wave_forces		1;
	data	apply_wind_forces		0;
	data	output	position		52;						    Write global position of node number <node> to output file
	data	output	force			52;						    Write global force of node number <node> to output file
	data	output	position		1;						    Write global position of node number <node> to output file
	data	output	force			1;						    Write global force of node number <node> to output file	
	data	end;
end	ext_sys;
```

The other two mooring lines have a similar block, however they are positioned at 120 degrees offset.

![Initial position of the lines, lying on the seabed, before the initialization procedure.](init_start.png)


The following commands are key in the example. They state that the final position of the lines will be stored in a file, in this case named `lineiniticond.dat`

```
data	read_write_initcond		0				1;			Read or write initial line geometry from file
data    read_write_initcond_file   ./lineinitcond.dat;      File name for the initial line geometry
```

The contraint of the initialization also plays an important part of the setup. In this constraint, we specify that the line constraint needs to be satisfied at `t = 50 [s]`, when the first line must be hooked to the 7th node of the central column. In the meanwhile, the column is kept locked to the global reference system by a `fix0` constraint so that the final position of the lines does not vary.

```
begin 	dll;
	ID				0	0	0	50.;
	dll				.\ESYSMooring.dll;
	init   			cstrbarsfixedtobodyrelative_init;
	update 			cstrbarsfixedtobodyrelative_update;
	neq 			3;
	nbodies 		1;
	nesys 			1;
	mbdy_node 		centralcolumn 7;
	esys_node 		line1 1;
end dll;
```

We run the simulation by 

```cmd
HAWC2MB.exe ESYSMooring_init.htc
``` 

The lines will slowly travel to their final position, reached at `t = 50[s]`. A file named `./lineinitcond.dat; ` is written, and the final position of the nodes stored.

The lines are now connected to the column.
You can visualize the procedure via our animation and visualization tools.

![Final position of the lines after the initialization procedure.](init.png)

The final position is stored in the `lineinitcond.dat` file. The format is quite simple: it just stores the coordinate of the nodes in a plain text space-separated file. In fact, it is quite easy to produce this file manually or by scripting, if a particular initial condition of the lines is needed.

```
 -8.065060512720034E-009 -1.554127974580027E-003   4.20000908125780     
  5.158310432728181E-007  -3.12477610935331        18.9719149344788     
  3.868274128938725E-007  -6.45541378861492        33.6984104749074     
 -1.856200538112021E-007  -10.0346908141741        48.3664266041105     
  9.501410589628056E-008  -13.9212533937797        62.9560002529595     
  1.627215248904091E-006  -18.1949308257180        77.4368780219794     
  1.667723972264061E-006  -22.9576952485593        91.7643109456053     
 -6.125843191060105E-007  -28.3229728433493        105.877148567282     
  1.947682963953227E-006  -34.3966594117915        119.699878623126     
  7.121322685391505E-006  -41.2716870115633        133.142002561953     
 -1.723907543806378E-006  -49.0473559988864        146.083983249180     
 -5.450945808193473E-006  -57.8564213819367        158.345936032741     
  9.605509649203206E-006  -67.8825795359462        169.634450007681     
 -1.878730437209499E-006  -79.3186294482940        179.491960170688     
 -8.547105673818650E-006  -92.2513617748657        187.282828867025     
  6.567652537783555E-006  -106.522880820261        192.209952934518     
 -1.103698315969897E-005  -121.566594801110        193.490297962115  
 ...   
```


## Production simulation

The production simulation is now ready.
The main difference in the line definition block lies in the first of the following two lines. Here we specify that the coordinates must be read by the file that we have previously created.

```
data	read_write_initcond		1				0;			Read or write initial line geometry from file
data    read_write_initcond_file   ./lineinitcond.dat;      File name for the initial line geometry
```

The constraint file is also the same as before, however this time we'll reduce the time-to-satisfy-constraint number to something small, as the line is already in the required position. 

```
begin 	dll;
	ID				0	0	0	1.0;
	dll				.\ESYSMooring.dll;
	init   			cstrbarsfixedtobodyrelative_init;
	update 			cstrbarsfixedtobodyrelative_update;
	neq 			3;
	nbodies 		1;
	nesys 			1;
	mbdy_node 		centralcolumn 7;
	esys_node 		line1 1;
end dll;
```
The column is now supposed to be floating freely, as the `fix0` constraint has been removed from the main `.htc` file.

We now run the production simulation by

```cmd
HAWC2MB.exe ESYSMooring_prod.htc
``` 

The column moves slowly at the heave natural period, as the column was (intentionally) not in equlibrium at the initial position. It reaches equilibrium towards the end of the simulation. The mooring lines have a nice and smooth shape.

