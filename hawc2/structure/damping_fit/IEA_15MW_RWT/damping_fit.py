#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Damping fit for HAWC2
This script finds the parameters for structural HAWC2 damping type
"damp_file", based on the mode damping matrix, A, calculated by HAWC2
and written to file "dfit_a.bin".
Each row of the A matrix corresponds to a mode shape in the HAWC2 model,
ordered in increasing order of eigenfrequency, i.e. first row corresponds to
the mode with the lowest eigenfrequency. By multiplication with the damping
parameter vector, x, gives the damping rati0 vector, d = (A*x).
The purpose of this script is then to find the best fit of x which gives the
specified damping ratio for the individual modes using the constraint that
x>0 for all x.
The A matrix contains all modes, and not all modes can be fitted for any
damping level. Normally the first (say 10) modes are of interest. This is
handled by the weighting vector, w, below. See code below for further details.
"""
import numpy as np
import struct
from scipy.optimize import nnls

wmin = 1.e-6

def read_struct():

    f=open('dfit_a.bin','rb')
    (nr,nc) = struct.unpack('ii',f.read(8))
    ntot = nr*nc
    data = np.zeros(ntot, dtype=np.dtype('f8'))
    for i in range(ntot):
        (data[i],) = struct.unpack('d',f.read(8))
    A = np.reshape(data,[nr,nc], order='F')
    # Read target damping for optimization
    d = np.zeros(nr,dtype=np.dtype('f8'))
    for i in range(nr):
        (d[i],) = struct.unpack('d',f.read(8))
    w = np.zeros(nr,dtype=np.dtype('f8'))
    for i in range(nr):
        (w[i],) = struct.unpack('d',f.read(8))
        if w[i] == 0.0:
            w[i] = wmin
    f.close()
    return A, d, w, nr, nc


def read_numpy():

    with open('dfit_a.bin', 'rb') as fid:
        nr, nc = np.fromfile(fid, dtype='i', count=2)
        A = np.fromfile(fid, dtype='f8', count=nr*nc).reshape((nr, nc), order='F')
        d = np.fromfile(fid, dtype='f8', count=nr)
        w = np.fromfile(fid, dtype='f8', count=nr)
        w = np.where(w==0.0, wmin, w)

    return A, d, w, nr, nc


def damping_fit():
    wmin = 1.e-6

    # Read A matrix from file
    A, d, w, nr, nc = read_numpy()
    # A, d, w, nr, nc = read_struct()

    # Solve
    res = nnls(np.matmul(np.diag(w),A), np.matmul(np.diag(w),d))

    # Write results to file
    f = open('dfit_x.bin','wb')
    f.write(np.array([nc,1],dtype='i4'))
    f.write(res[0])
    f.close()

    # Check solution
    dfit = np.matmul(A,res[0])
    print(('*'+'{0:1s}'*36+'*').format('*'))
    print('*{:^36s}*'.format('Damping fit result'))
    print(('*'+'{0:1s}'*36+'*').format('*'))
    print('*{:^8s}{:^14s}{:^14s}*'.format('Mode','Target','Fit'))
    for i in range(nr):
        if w[i] > wmin:
            print('*{:^8d}{:^14.3e}{:^14.3e}*'.format(i+1,d[i],dfit[i]))
    print(('*'+'{0:1s}'*36+'*').format('*'))

    return res


if __name__ == '__main__':

    res = damping_fit()

    # A1, d1, w1 = read_struct()
    # A2, d2, w2 = read_numpy()
    # print(np.allclose(A1, A2))
    # print(np.allclose(d1, d2))
    # print(np.allclose(w1, w2))
