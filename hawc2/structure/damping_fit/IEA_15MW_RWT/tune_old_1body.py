# -*- coding: utf-8 -*-
import os
import subprocess as sproc
from glob import glob

import numpy as np
#from matplotlib import pyplot as plt

from wetb import hawc2
from wetb.prepost import windIO
#from wetb.prepost.windIO import ReadEigenStructure
from wetb.prepost.windIO import ReadEigenBody

from openmdao.api import Problem, ScipyOptimizeDriver, IndepVarComp, ExplicitComponent


class HAWC2DampTuningExec(ExplicitComponent):
    """Simple wind turbine model based on actuator disc theory"""

    def initialize(self, nr_modes=2, htc_fname='opt.htc', body_name='blade1',
                   damp_cmd='damping_posdef'):
        self.options.declare('nr_modes', default=nr_modes,
                             desc="number of modes to consider")
        self.options.declare('htc_fname', default=htc_fname,
                             desc="htc file name as used by optimizer")
        self.options.declare('body_name', default=body_name,
                             desc="body name in htc file")
        self.options.declare('damp_cmd', default=damp_cmd,
                             desc="Command used for the damping")

        # user configuration
        cmd = 'WINEPREFIX=~/.wine32 WINEARCH=win32 wine HAWC2MB.exe'
        self.options.declare('hawc2_cmd', default=cmd)

    def setup(self):
        # Inputs
#        self.add_input('mx', desc="flap-wise mass damping coeff")
#        self.add_input('my', desc="edge-wise mass damping coeff")
#        self.add_input('mz', desc="torsion mass damping coeff")
        self.add_input('kx', desc="flap-wise stiff damping coeff")
        self.add_input('ky', desc="edge-wise stiff damping coeff")
        self.add_input('kz', desc="torsion stiff damping coeff")
        self.add_input('damp_ref', shape=self.options['nr_modes'],
                       desc="target damping values in log decr pct")
        self.add_output('damp', shape=self.options['nr_modes'],
                        desc="current log decr damping in pct")
        self.add_output('obj', desc="Objective function output")

        # Finite difference all partials.
        self.declare_partials('*', '*', method='fd')

    def cost_function(self, ref_val, new_val):
        """It is important that the cost function returns a value that
        approaches 0 for the optimal answer. Ideally, it returns values close
        to 1 (and not larger) when the answer is sub-optimal.
        """

        obj = np.abs(1-(new_val/ref_val))
#        # 2 first modes count 3x, 2 second 2x
#        obj[:2] *= 15.0
#        obj[2:4] *= 6.0
#        obj[4:10] *= 2.0
        # modes 5, 6 and 7 count only for 20%
        obj[4:7] *= 0.2
        obj[8:10] *= 0.1

        return np.sum(obj**2)

    def compute(self, inputs, outputs):
        """ Considering the entire rotor as a single disc that extracts
        velocity uniformly from the incoming flow and converts it to
        power."""

        damp_ref = inputs['damp_ref']
        htc_fname = self.options['htc_fname']
        body_name = self.options['body_name']
        hawc2_cmd = self.options['hawc2_cmd']
        nr_modes = self.options['nr_modes']
        damp_cmd = self.options['damp_cmd']

#        dcoef = np.ndarray((6,), dtype=np.float32)
#        dcoef[0] = inputs['mx']
#        dcoef[1] = inputs['my']
#        dcoef[2] = inputs['mz']
#        dcoef[3] = inputs['kx']
#        dcoef[4] = inputs['ky']
#        dcoef[5] = inputs['kz']

        dcoef = np.ndarray((6,), dtype=np.float32)
        dcoef[0] = 0
        dcoef[1] = 0
        dcoef[2] = 0
        dcoef[3] = inputs['kx']
        dcoef[4] = inputs['ky']
        dcoef[5] = inputs['kz']

        # write input file, run and get response
        htcfile = os.path.join(os.getcwd(), htc_fname)
        htc = hawc2.HTCFile(htcfile)
        bodies = htc.contents['new_htc_structure']
        body = bodies.get_subsection_by_name(body_name)
        body[damp_cmd].values = dcoef.tolist()

#        # set the analysis type: structural eigenvalue analysis
#        fname_eigen = htc_fname.replace('.htc', '_structure.dat').lower()
#        bodies['structure_eigenanalysis_file_name'].values = [fname_eigen]

        # save modified htc file
        htc.save(filename=os.path.join(os.getcwd(), htc_fname))

        print('-'*79)
        print(dcoef)

        # ---------------------------------------------------------------------
        # The command depends on your operating system and where the users
        # has actually saved/stored and named the HAWC2 executable.
        cmd = '%s %s' % (hawc2_cmd, htc_fname)
        # we pipe both std out and err to the same file
        p = sproc.Popen(cmd, stdout=sproc.PIPE, stderr=sproc.STDOUT, shell=True)
        p.wait()
        output = p.stdout.readlines()
        # try:
        #     errors = p.stderr.readlines()
        # except:
        #     errors = ''
        print('='*79)
        print('HAWC2 output to command line (standard out, error)')
        print('='*79)
        print(output)
        print('='*79)

        # ---------------------------------------------------------------------
        # CHECK IF THERE WAS ANY ERROR IN THE LOG FILE!!
        logf = windIO.LogFile()
        fname_log = os.path.join(os.getcwd(), htc_fname.replace('.htc', '.log'))
        logf.readlog(fname_log)
        contents = logf._msglistlog2csv('')
        if contents.lower().find('error') > -1:
            raise UserWarning('Error in logfile. Fix your htc file first!')

        # ---------------------------------------------------------------------
        # read the result of the analysis
#        fname_struct = htc_fname.replace('.htc', '_structure.dat').lower()
#        syseigenstruct = ReadEigenStructure('', fname_struct)
        fname_body = htc_fname.replace('.htc', '_body_eigen.dat').lower()
        bodyeigen_all = ReadEigenBody(fname_body)
        bodyeigen = bodyeigen_all[bodyeigen_all['body']==body_name]

        # ---------------------------------------------------------------------
        # clean-up
        for k in glob(os.path.join(os.getcwd(), 'mode0*')):
            # print(k)
            os.remove(k)

        # ---------------------------------------------------------------------
#        damp_new = syseigenstruct[2,:nr_modes]
        damp_new = bodyeigen['log_decr_pct'].values[:nr_modes]

        # calculate the cost/objective function based on the reference values
        # and the newly calculated values
        outputs['obj'] = self.cost_function(damp_ref, damp_new)

        outputs['damp'] = damp_new

        # add print statements in case you want to see what happens at each
        # compute execution
#        print('-'*79)
#        print(dcoef)
        print(damp_new)
        print(outputs['obj'])
#        print('damping coefficients', prob['kx'], prob['ky'], prob['kz'])
#        print('damping log decr pct', prob['damping.damp'])
#        print('  objective function', prob['damping.obj'])


def prepare_htc_opt(fname, fname_opt, body_name, damp_cmd):
    """Prepare the htc file for running the optimisation: get the initial
    damping parameters from the reference htc file, and activate the eigenvalue
    analysis.
    """


    fpath = os.path.join(os.getcwd(), fname)
    htc = hawc2.HTCFile(os.path.basename(fpath), modelpath=os.path.dirname(fpath))
    bodies = htc.contents['new_htc_structure']
    body = bodies.get_subsection_by_name(body_name)
    dcoef_init = body[damp_cmd].values

#    fname_structure = fname_opt.replace('.htc', '_structure.dat')
#    bodies['structure_eigenanalysis_file_name'].values = [fname_structure]

    fname_body = fname_opt.replace('.htc', '_body_eigen.dat')
    bodies['body_eigenanalysis_file_name'].values = [fname_body]

    simulation = htc.contents['simulation']
    simulation['logfile'].values = [fname_opt.replace('.htc', '.log')]

    # Overwrite some other starting values for the damping
#    dcoef = np.ndarray((6,), dtype=np.float32)
#    dcoef[0] = 0
#    dcoef[1] = 0
#    dcoef[2] = 0
#    dcoef[3] = 0.01
#    dcoef[4] = 0.01
#    dcoef[5] = 0.01
#    body = bodies.get_subsection_by_name(body_name)
#    body[damp_cmd].values = dcoef.tolist()
    # ----------------------------------------------------

    htc.save(filename=os.path.join(os.getcwd(), fname_opt))

    return htc, dcoef_init


def optimize():

    # =========================================================================
    # USER INPUT
    # =========================================================================
#    # number of modes to consider, will affect HAWC2DampTuningExec.cost_function
#    nr_modes = 7
#    # set realistic target damping values! The optimization might fail if they
#    # aren't
#    damp_ref = np.ones((nr_modes,), dtype=np.float32) * 1.5

    # for the shaft, set torsional target of 1.5% log decr

    # ------------------------------------------
    # select body name and damping type
    body_name = 'blade1'
    damp_cmd = 'damping_aniso_v2'

    # select target damping levels:
    # ----------------------------------
    # Option A: from an old damping file
    # fname_body = 'IEA_15MW_RWT_blade_1body_tune_old_body_eigen.dat'
    # bodyeigen_all = ReadEigenBody(fname_body)
    # bodyeigen = bodyeigen_all[bodyeigen_all['body']==body_name]
    # damp_ref = bodyeigen['log_decr_pct'].values[0:11]
    # ----------------------------------
    # Option B: simply specify the log.decr in % for a series of modes
    damp_ref = np.array([3.0, 3.0, 8.4, 8.9, 17.0, 20.8, 26.3, 4.99, 41.8])

    # ------------------------------------------
    # tower:
#    body_name = 'tower'
#    damp_cmd = 'damping_posdef'
#    damp_ref = np.array([4.3, 4.3, 21.0, 21.0])
    # ------------------------------------------

    nr_modes = len(damp_ref)

    # reference htc file name
    fname_ref = 'htc/IEA_15MW_RWT_blade_1body_tune_old.htc'
    # target body name in the reference htc file

    # htc file name used by the optimizer
    fname_opt = 'opt.htc'
    # upper and lower bounds for the damping coefficients
    upper = 1e-02
    lower = 5e-07
    # =========================================================================

    # read the reference model
    htc, dcoef_init = prepare_htc_opt(fname_ref, fname_opt, body_name, damp_cmd)

    prob = Problem()
    indeps = prob.model.add_subsystem('indeps', IndepVarComp(), promotes=['*'])

    # set the inital values of the damping coefficients as taken from the
    # reference htc file fname_ref
#    indeps.add_output('mx', dcoef_init[0])
#    indeps.add_output('my', dcoef_init[1])
#    indeps.add_output('mz', dcoef_init[2])
    indeps.add_output('kx', dcoef_init[3])
    indeps.add_output('ky', dcoef_init[4])
    indeps.add_output('kz', dcoef_init[5])
    indeps.add_output('damp_ref', val=damp_ref)

    kwargs = {'nr_modes' : nr_modes,
              'htc_fname' : 'opt.htc',
              'body_name' : body_name,
              'damp_cmd'  : damp_cmd}
    prob.model.add_subsystem('damping', HAWC2DampTuningExec(**kwargs),
                             promotes_inputs=['damp_ref', 'kx', 'ky', 'kz',
#                                              'mx', 'my', 'mz',
                                              ])

    # setup the optimization
    prob.driver = ScipyOptimizeDriver()
    prob.driver.options['optimizer'] = 'SLSQP'

#    prob.model.add_design_var('mx', lower=lower, upper=upper)
#    prob.model.add_design_var('my', lower=lower, upper=upper)
#    prob.model.add_design_var('mz', lower=lower, upper=upper)
    prob.model.add_design_var('kx', lower=lower, upper=upper)
    prob.model.add_design_var('ky', lower=lower, upper=upper)
    prob.model.add_design_var('kz', lower=lower, upper=upper)
    # negative one so we maximize the objective
    prob.model.add_objective('damping.obj')#, scaler=-1)

    prob.setup()
    prob.run_driver()

    # print the final optimized values
    print('='*79)
    print('Final values')
#    print(prob['mx'], prob['my'], prob['mz'], prob['kx'], prob['ky'], prob['kz'])
    print('damping coefficients', prob['kx'], prob['ky'], prob['kz'])
    print('damping log decr pct', prob['damping.damp'])
    print('  objective function', prob['damping.obj'])
    print('='*79)


if __name__ == '__main__':

    optimize()
