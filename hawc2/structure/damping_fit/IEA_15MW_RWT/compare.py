# -*- coding: utf-8 -*-
"""
Example showing how one could use and tune damping with the damping_fit
procedure in HAWC2. The following procedure was followed for this example:

    * determine target damping values: simply use the damping resulting
    from using the damping_aniso inputs as defined in htc/IEA_15MW_RWT_blade_rd.htc
    and htc/IEA_15MW_RWT_full_rd.htc

    * convert the log decr % damping from the structural eigenvalue analysis
    and enter them as damping targets in htc/IEA_15MW_RWT_blade_make_damping_fit.htc
    and htc/IEA_15MW_RWT_full_make_damping_fit.htc. Note that for the full
    turbine analysis only the modes are selected that relate to the blade. This
    requires visual inspection of the animation files of the modes, or match
    the blade modes from the blade only analysis with the onces from the full
    turbine analysis based on the best match on both frequency and damping.

    * execute damping fitting procedure by runing
    htc/IEA_15MW_RWT_blade_make_damping_fit.htc and
    htc/IEA_15MW_RWT_full_make_damping_fit.htc

    * compute frequencies and damping of the fitted damping model by running
    htc/IEA_15MW_RWT_blade.htc and htc/IEA_15MW_RWT_full.htc

    * plot the results using the method compare_target_vs_fitted_values().
"""

import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

# from wetb.prepost.windIO import ReadEigenStructure
def ReadEigenStructure(fname):
    return pd.read_fwf(fname, colspecs=[(8,11), (13,27), (27,42), (42,57)],
                       skiprows=8, names=['mode_nr', 'Fd_hz', 'Fn_hz', 'log_decr_pct'])


def logdec2ratio(logdecr):
    """Convert from damping ratio to logarithmic decrement


    Returns
    -------
    None.

    """

    return 1 / (np.sqrt(1 + (2*np.pi / logdecr)**2))


def prepare_fitting():
    # make damping fit based on the current rd (damping_aniso) results
    structeigen_b_rd = ReadEigenStructure('bodyeig/iea_15mw_rwt_blade_rd/struc_eigen.dat')
    rd_dr = logdec2ratio(structeigen_b_rd['log_decr_pct'].values[:50]/100)
    print('blade only RD (damping_aniso) results')
    for i, k in enumerate(rd_dr):
        print(f'mode {i+1} {k};')

    structeigen_f_rd = ReadEigenStructure('bodyeig/iea_15mw_rwt_full_rd/struc_eigen.dat')
    rd_dr = logdec2ratio(structeigen_f_rd['log_decr_pct'].values[:50]/100)
    print('full turbine RD (damping_aniso) results')
    for i, k in enumerate(rd_dr):
        print(f'mode {i+1} {k};')


def compare_target_vs_fitted_values():

    # freq/damping based on the damping_aniso damping model
    structeigen_b_rd = ReadEigenStructure('bodyeig/iea_15mw_rwt_blade_rd/struc_eigen.dat')
    structeigen_f_rd = ReadEigenStructure('bodyeig/iea_15mw_rwt_full_rd/struc_eigen.dat')

    # freq/damping based on the fitted damping element matrix
    structeigen_b = ReadEigenStructure('bodyeig/iea_15mw_rwt_blade/struc_eigen.dat')
    structeigen_f = ReadEigenStructure('bodyeig/iea_15mw_rwt_full/struc_eigen.dat')

    # only consider the first 50 modes in this example
    # fnbrd = structeigen_b_rd['Fn_hz'].values[:50]
    dlbrd = structeigen_b_rd['log_decr_pct'].values[:50]

    # fnfrd = structeigen_f_rd['Fn_hz'].values[:50]
    dlfrd = structeigen_f_rd['log_decr_pct'].values[:50]

    # fnb = structeigen_b['Fn_hz'].values[:50]
    dlb = structeigen_b['log_decr_pct'].values[:50]

    # fnf = structeigen_f['Fn_hz'].values[:50]
    dlf = structeigen_f['log_decr_pct'].values[:50]

    # plt.figure('damping fit vs rd (damping_aniso) for blade  (frequencies)')
    # plt.title ('damping fit vs rd (damping_aniso) for blade  (frequencies)')
    # plt.plot(fnbrd, dlbrd, 'rs', label='rd blade')
    # plt.plot(fnb, dlb, 'kx', label='fit blade')
    # plt.xlabel('Fn [Hz]')
    # plt.legend(loc='best')
    # plt.grid()

    targets = np.array([1,2,3,4,5,6,7,8,9,10])#,20,21,22,23,25,28])
    plt.figure('damping fit vs rd (damping_aniso) for blade (mode numbers)')
    plt.title ('damping fit vs rd (damping_aniso) for blade (mode numbers)')
    plt.plot(dlbrd, 'rs', label='rd blade', ms=7, alpha=0.7)
    plt.plot(targets-1, dlbrd[targets-1], 'wo', label='targets', mec='k')
    plt.plot(dlb, 'kx', label='fit blade')
    plt.xlabel('mode number')
    plt.ylabel('damping log.decr %')
    plt.legend(loc='best')
    plt.grid()
    plt.savefig('damping_rd_vs_fit_blade_modenumbers.png')

    # plt.figure('damping fit vs rd (damping_aniso) for full (frequencies)')
    # plt.title ('damping fit vs rd (damping_aniso) for full (frequencies)')
    # plt.plot(fnfrd, dlfrd, 'rs', label='rd full')
    # plt.plot(fnf, dlf, 'kx', label='fit full')
    # plt.xlabel('Fn [Hz]')
    # plt.legend(loc='best')
    # plt.grid()

    targets = np.array([6,7,8,12,15,20,23,24,25,26,29,30,34,35,36,37])
    plt.figure('damping fit vs rd (damping_aniso) for full (mode numbers)')
    plt.title ('damping fit vs rd (damping_aniso) for full (mode numbers)')
    plt.plot(dlfrd, 'rs', label='rd full', ms=7, alpha=0.7)
    plt.plot(targets-1, dlfrd[targets-1], 'wo', label='targets', mec='k')
    plt.plot(dlf, 'kx', label='fit full')
    plt.xlabel('mode number')
    plt.ylabel('damping log.decr %')
    plt.legend(loc='best')
    plt.grid()
    plt.savefig('damping_rd_vs_fit_fullturbine_modenumbers.png')


if __name__ == '__main__':

    prepare_fitting()
    compare_target_vs_fitted_values()
