# Simple beam

## Introduction

This example contains a model of a simple beam that is using three different mass distributions. 
The intention is to demonstrate what effect `mass_scale_method` (as defined in the `timoschenko_input) has on
total mass and static moment around the first node. See also section "Sub sub command block – timoschenko_input" in the HAWC manual.

There are three different mass distributions in `simplebeam.st` and they all have the same total integrated mass:
* subset 1: increasing linearly toward the tip (/)
* subset 2: linearly in/de-creasing towards the middle (/\)
* subset 3: two constant levels (_-)

By comparing the output of the corresponding htc files `simplebeam1.htc` to `simplebeam3.htc`
one can note how the actual mass of the each the three different examples changes
as well as the static moment (due to gravity) around the first node.

The results are summarized in the following table:

| |Subset 1|Subset 2|Subset 3|
|--------|--------|--------|--------|
|`st` integrated [kg]|30.000|30.000|30.000|
|HAWC2 body `mass_scale_method=1` [kg]|32.778|27.361|30.000|
|HAWC2 body cg [m]|2.924|1.999|3.021|
|HAWC2 node 1 Mx [Nm]|-940.710|-536.829|-889.565|


## Running the examples

The examples can be run using the following command lines:

Example using subset 1:

```cmd
HAWC2MB.exe simplebeam1.htc
```

Example using subset 2:

```cmd
HAWC2MB.exe simplebeam2.htc
```

Example using subset 3:

```cmd
HAWC2MB.exe simplebeam3.htc
```
