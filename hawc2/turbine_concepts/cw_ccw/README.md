# Converting a CW rotating rotor into a CCW one

Here's a summary of the changes that are needed in order to turn a standard upwind clockwise (CW) rotating turbine into a counter-clockwise (CCW) turning version (and that will otherwise operate identical):

## In the HTC file

### Blade body, c2_def section:
 
* flip sign on z-axis coordinates (so positive towards tip for CW, while negative towards tip for CCW)
* flip sign on twist/theta
* see lines 165-183 from the example

### Direction of rotation of the rotor bearing (omega)
 
* flip sign on omega (both init vector and positive rotation direction definition)
* see lines 215 and 276 from the example

### Blade coordinate system orientation
 
* rotate 180 along y-axis wrt hub coordinate system
* see lines 245, 251 and 257 in the example

### Aero section
 
* add one command (HAWC2 13.0 and later): `rotate_sec 0.0 180.0 0.0;` rotation of aero sections relative to default orientation
* see line 358 in the CCW example
 
### DTU Wind Energy Controller
 
* block for `generator_servo.dll`: change sign for the shaft moment under `output` (4th output) and `actions` (1st action)
* block for `mech_brake.dll`: change sign for the shaft moment under `actions`
* not shown in current example


### Wind section

* change sign of the yaw error (see line 331 of the example)


### In the st file

The following values should have the opposite sign for a CCW rotor:

```
column    Parameter  
   3          x_cg  (center of gravity)
   7          x_sh  (shear center)
  17       theta_z  (pitch)
  18           x_e  (elastic axis)
```



