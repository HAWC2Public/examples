# Earthquake simulation

## Introduction

This example contains htc files of the NREL 5MW Reference turbine and a simple beam exposed to an earthquake. The example can be run with HAWC2 for any platform.

## Modelling

The earthquake is modelled as an acceleration time series applied at a given node. The implementation allows for the user to specify the acceleration in all 3 spatial coordinates at any point in the time series.

It should be noted that if the acceleration does not integrate to 0.0 over the specified time series, there will be a residual velocity and thus a drift in the position of the model. This phenomenon is purely numerical, and also occurs in measurements due to innacuracies. An example of such a time series is shown below.

![Position, velocity and acceleration of the tower base.](esys_mck_earthquake_v1.1_zoomed.png)

Timeseries can be balanced using the function `balance_timeseries()` in the python file `balance_timeseries.py`. This function balances the timeseries by shifting it to force the integrated acceleration to 0. When using the balanced time series, you may still see a change in initial and final position of the node on which the accelerations are applied.

## Specifying the time series

The time series of the acceleration is done in a plain text file as space-separated-values. The 0.5 second of an example time series is shown below. Note that the time series does not need to start at 0, nor end at the end of the simulation.

```
    T      accx    accy    accz     (sek, m/s2) x down, y lat. z long.
 30.000    0.000  -0.000  -0.000
 30.390    0.000   0.000   0.000
 30.400    0.000  -0.000   0.001
 30.410    0.000  -0.000   0.001
 30.420    0.000   0.000   0.001
 30.430    0.000   0.001   0.001
 30.440    0.000   0.001   0.001
 30.450    0.000   0.001   0.001
 30.460    0.000   0.002   0.000
 30.470    0.000   0.002   0.000
 30.480    0.000   0.003   0.000
 30.490    0.000   0.003   0.001
 30.500    0.000   0.004   0.001
```

## Reading the time series in HAWC2

The file containing the time series is sent to HAWC2 via the htc-file. The external system (ESYS) is defined in its own block under the `new_htc_structure` block. An example of the definition is shown below. Note here that the line `data  FORCING_FILE earthquake_acc_field.qda 20` allows the user to skip a number of lines from the time series file, allowing for comments and descriptions of the time series. 

```
  begin ext_sys ;
    name earthquake_sys                 ; Name of this instance
    module esys_mck                     ; Module name (must be "esys_mck")
    dll  .\ESYSTools.dll                ; Path and name of present DLL
    ndata 6                             ; NOF data lines below
    data  NEQ 3                         ; NOF equations (MUST be the first line)
    data  MASS 1 1 1.0                  ; Mass matrix component, M(irow,icol)=value
    data  MASS 2 2 1.0                  ; Mass matrix component, M(irow,icol)=value
    data  MASS 3 3 1.0                  ; Mass matrix component, M(irow,icol)=value
    data  FORCING_FILE earthquake_acc_field.qda 20  ; Read acceleration (rhs) from file (ignore 20 first lines). 1st column is time, and next <NEQ> columns are accelerations
    data  SCALE_RESIDUAL 1.e+06         ; Scale residual to account for absolute convergence criterion used by HAWC2
  end ext_sys ;
```


The application of the time series onto a node in the HAWC2 model is done in `dll` blocks. Examples of such blocks are shown below. These three blocks apply the accelerations on to the `main_body` `tower`, and does so for DOF 1,2 and 3 (the x, y and z directions).

```
   begin dll;
     ID        1.0 0.0 0.0                    ; Global direction in which the body node is forced (according to ESYS state)
     dll       .\ESYSTools.DLL                ; name of DLL
     init      cstr_body_origin_disp_forced_init     ; name of init subroutine in DLL (must be "cstr_body_origin_disp_forced_init")
     update    cstr_body_origin_disp_forced_update   ; name of update subroutine in DLL (must be "cstr_body_origin_disp_forced_update")
     neq       1                              ; NOF equations       (must be 1)
     nbodies   1                              ; NOF bodies involved (must be 1)
     nesys     1                              ; NOF ESYSs involved  (must be 1)
     mbdy_node tower  0                       ; name of body (node index is dummy)
     esys_node earthquake_sys 1               ; name of ESYS and DOF index. 
 end dll;
;
   begin dll;
     ID        0.0 1.0 0.0                    ; Global direction in which the body node is forced (according to ESYS state)
     dll       .\ESYSTools.DLL                ; name of DLL
     init      cstr_body_origin_disp_forced_init     ; name of init subroutine in DLL (must be "cstr_body_origin_disp_forced_init")
     update    cstr_body_origin_disp_forced_update   ; name of update subroutine in DLL (must be "cstr_body_origin_disp_forced_update")
     neq       1                              ; NOF equations       (must be 1)
     nbodies   1                              ; NOF bodies involved (must be 1)
     nesys     1                              ; NOF ESYSs involved  (must be 1)
     mbdy_node tower  0                       ; name of body (node index is dummy)
     esys_node earthquake_sys 2               ; name of ESYS and DOF index. 
 end dll;
;
   begin dll;
     ID        0.0 0.0 1.0                    ; Global direction in which the body node is forced (according to ESYS state)
     dll       .\ESYSTools.DLL                ; name of DLL
     init      cstr_body_origin_disp_forced_init     ; name of init subroutine in DLL (must be "cstr_body_origin_disp_forced_init")
     update    cstr_body_origin_disp_forced_update   ; name of update subroutine in DLL (must be "cstr_body_origin_disp_forced_update")
     neq       1                              ; NOF equations       (must be 1)
     nbodies   1                              ; NOF bodies involved (must be 1)
     nesys     1                              ; NOF ESYSs involved  (must be 1)
     mbdy_node tower  0                       ; name of body (node index is dummy)
     esys_node earthquake_sys 3               ; name of ESYS and DOF index. 
 end dll;

```

## Running the example

The examples can be run using the following command lines:

Example using the NREL 5MW Reference turbine with an unbalanced acceleration input:

```cmd
HAWC2MB.exe esys_mck_earthquake.htc
```

Example using a single tower with a balanced acceleration input:

```cmd
HAWC2MB.exe esys_mck_earthquake_simple.htc
```
