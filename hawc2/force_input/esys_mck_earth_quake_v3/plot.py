# -*- coding: utf-8 -*-
"""
Created on Tue May 31 14:18:23 2016

@author: dave
"""

import os
#from argparse import ArgumentParser
#from multiprocessing import Pool
#from itertools import product
#from glob import glob
import numpy as np
#import pandas as pd
from matplotlib import pyplot as plt
import matplotlib as mpl
import matplotlib.ticker as ticker
from wetb.prepost import (windIO, mplutils)


def read_hawc2(fname):
    p1 = os.path.dirname(fname)
    p2 = os.path.basename(fname)

    try:
        res = windIO.LoadResults(p1, p2)
    except Exception as e:
        print('failed loading:', fname)
        print(e)
        return

    df = res.ch_df.copy()
    df['chi'] = df.index
    df.set_index('unique_ch_name', inplace=True)
    return res.sig2df(), df


def dashboard2(fnames, labels=None, figname=None):

    if not isinstance(fnames, list):
        fnames = [fnames]
    if not isinstance(labels, list):
        labels = [labels]

    nrows = 6
    ncols = 3
    fig, axes = mplutils.subplots(nrows=nrows, ncols=ncols, figsize=(22,14),
                                  dpi=120)

    colors = [['k-','k-'], ['b-','b-'], ['r--','r--']]
    alphas = [1.0, 0.6, 0.5]

    for fname, color, alpha, label in zip(fnames, colors, alphas, labels):
        print(fname, label)
        dashboard(fname, axes, colors=color, alpha=alpha, label1=label)

    fig.suptitle(fname)
    fig.tight_layout()
    fig.subplots_adjust(top=0.92)
    if figname is None:
        figname = fname + '.png'
    print('saving: %s' % (figname))
    fig.savefig(figname)


def dashboard(fname, axes, colors=['k-','r-'], alpha=1.0, label1=None):

    p1 = os.path.dirname(fname)
    p2 = os.path.basename(fname)

    try:
        res = windIO.LoadResults(p1, p2)
    except Exception as e:
        print('failed loading:', fname)
        print(e)
        return

    df = res.ch_df.copy()
    df['chi'] = df.index
    df.set_index('unique_ch_name', inplace=True)
    time = res.sig[:,0]

#    for k in sorted(df.index): print(k)

    plot_chans = {}
#    plot_chans['RPM'] = ['bearing-shaft_rot-angle_speed-rpm']
#    plot_chans['$P_e$'] = ['DLL-generator_servo-inpvec-2']
    plot_chans['$TB_{SS}$'] = ['tower-tower-node-001-momentvec-y']
    plot_chans['$TB_{FA}$'] = ['tower-tower-node-001-momentvec-x']
    plot_chans['$BN4_{flap}$'] = ['blade1-blade1-node-004-momentvec-x']
    plot_chans['$BN4_{edge}$'] = ['blade1-blade1-node-004-momentvec-y']
    plot_chans['$BN4_{torsion}$'] = ['blade1-blade1-node-004-momentvec-z']
    # plot_chans['$BN20_{flap}$'] = ['local-blade1-node-020-momentvec-x']
    # plot_chans['$BN20_{edge}$'] = ['local-blade1-node-020-momentvec-y']
    # plot_chans['$BN20_{torsion}$'] = ['local-blade1-node-020-momentvec-z']
#    plot_chans['$Q_{gen}$'] = ['DLL-generator_servo-inpvec-1']
    plot_chans['$shaft_{torsion}$'] = ['shaft-shaft-node-004-momentvec-z']
#    plot_chans['wdir'] = ['windspeed-global-Vdir_hor-0.00-0.00--119.00']
    # plot_chans['$\\alpha_{tip}$'] = ['Alfa-1-60.8']
#    plot_chans['$Torsion_{9.7}$'] = ['Tors_e-1-9.7']
#    plot_chans['$\\alpha_{9.6}$'] = ['Alfa-1-9.6']
#    plot_chans['$Torsion_{9.6}$'] = ['Tors_e-1-9.6']

    # add channels
    # new = np.zeros((res.sig.shape[0],1))
    # chan = 'tower-top-acc-xy'
    # ch1 = res.sig[:,df.loc['tower-tower-elem-021-zrel-1.00-State acc-x', 'chi']]
    # ch2 = res.sig[:,df.loc['tower-tower-elem-021-zrel-1.00-State acc-y', 'chi']]
    # new[:,0] = np.sqrt(ch1*ch1 + ch2*ch2)
    # res.sig = np.append(res.sig, new, axis=1)
    # df.loc[chan, 'chi'] = res.sig.shape[1] - 1
    # plot_chans['$TT_{xy,acc}$'] = [chan]

    # plots withouth PSD
    nonpsd = {}
#    nonpsd['$B1_{pitch}$'] = ['bearing-pitch1-angle-deg']
    nonpsd['$V_{hub}$'] = ['windspeed-global-Abs_vhor-0.00-0.00--119.00']
    nonpsd['$V_{dir}$'] = ['windspeed-global-Vdir_hor-0.00-0.00--119.00']
    # nonpsd['status'] = ['DLL-dtu_we_controller-inpvec-22']
#    nonpsd['yaw angle'] = ['bearing-yaw_ang-angle-deg']

    nonpsd['$BN20_{flap}$'] = ['local-blade1-node-020-momentvec-x']
    nonpsd['$BN20_{edge}$'] = ['local-blade1-node-020-momentvec-y']
#    nonpsd['yaw error'] = ['yaw_error']
    nonpsd['$\\alpha_{39}$'] = ['Alfa-1-39.1']
    nonpsd['$\\alpha_{tip}$'] = ['Alfa-1-60.76']

#    nonpsd['$TT_{acc}$'] = ['DLL-dtu_we_controller-inpvec-27']
#    nonpsd['brake'] = ['DLL-mech_brake-inpvec-1']
#    nonpsd['tower clearance'] = ['DLL-towerclearance_mblade-inpvec-1']
#    nonpsd['status'] = ['DLL-dtu_we_controller-inpvec-22']

    nrows = axes.shape[0]
    ncols = axes.shape[1]

    col = 0
    colors = colors*ncols*nrows
#    for i, (ax, (label, chans)) in enumerate(zip(axes, plot_chans.items()):
    for i, (label, chans) in enumerate(sorted(plot_chans.items())):
        row = int(np.floor(i/ncols))
        ax = axes[row*2, col]
        axp = axes[row*2+1, col]
        cs = colors[i]
#        print(i, row, col)
        chan = chans[0]
        sig = res.sig[:,df.loc[chan, 'chi']]
#        ax.set_title(chan.replace('_', '\\_'))
        ax.set_title(f'{label}: {chan}')
        if label1 is None:
            _label = label
        else:
            _label = label1
        ax.plot(time, sig, cs, label=_label, alpha=alpha)
        ax.set_xlim([time[0], time[-1]])
        # clear all the labels
        # ax.set_xticklabels([])
        # alternatively, make the labels invisible
        f0, f1, = 0, 2
        dfmi, dfma = 0.1, 0.5
        mpl.artist.setp(ax.get_xticklabels(), visible=False)
        axp = mplutils.psd(axp, time, sig, res_param=250, f0=f0, f1=f1,
                           nr_peaks=10, min_h=15, mark_peaks=True, col=cs,
                           label=None, alpha=alpha/2, ypos_peaks=0.9,
                           ypos_peaks_delta=0.12)
        axp.set_xlim([f0, f1])
        axp.set_yscale('log')
        axp.grid(True, which='minor')
        # set PSD plot to grey
        axp.spines['bottom'].set_color('grey')
        axp.spines['top'].set_color('grey')
        axp.spines['right'].set_color('grey')
        axp.spines['left'].set_color('grey')
        axp.tick_params(axis='x', colors='grey', which='both')
        axp.tick_params(axis='y', colors='grey', which='both')
        axp.xaxis.set_ticks(np.arange(f0, f1, dfma))
        axp.xaxis.set_ticks(np.arange(f0, f1, dfmi), minor=True)
        if row*2+1 < nrows-2:
            mpl.artist.setp(axp.get_xticklabels(), visible=False)
        else:
            axp.xaxis.set_major_formatter(ticker.FormatStrFormatter('%1.0f'))

        ax.grid(True)
        ax.legend(loc='best', borderaxespad=0)
        col += 1
        if col >= ncols:
            col = 0

    j = len(plot_chans)
    col = 0
    for i, (label, chans) in enumerate(sorted(nonpsd.items())):
        row = int(np.floor((i+j*2)/ncols))
        ax = axes[row, col]
        cs = colors[i+j]
        chan = chans[0]
        sig = res.sig[:,df.loc[chan, 'chi']]
        ax.set_title(f'{label}: {chan}')
        if label1 is None:
            _label = label
        else:
            _label = label1
        ax.plot(time, sig, cs, label=_label, alpha=alpha)
        ax.set_xlim([time[0], time[-1]])
        ax.grid(True)
        ax.legend(loc='best', borderaxespad=0)

        col += 1
        if col >= ncols:
            col = 0


if __name__ == '__main__':

    fig1, ax1 = plt.subplots(figsize=(12,3))
    fname = 'res/esys_mck_earthquake_v1.1'
    df, ch_df = read_hawc2(fname)
    time = df['Time']

    ch = 'global-tower-elem-001-zrel-0.00-State acc-x'
    ax1.plot(time, df[ch], 'k-', label='acc x')

    ch = 'global-tower-elem-001-zrel-0.00-State vel-x'
    ax1.plot(time, df[ch], 'r--', label='vel x')

    ch = 'global-tower-elem-001-zrel-0.00-State pos-x'
    ax1.plot(time, df[ch], 'y-.', label='pos x')

    ax1.grid()
    ax1.legend(loc='best')
    ax1.set_title('tower base pos, vel and acc')
    ax1.set_xlabel('time')
    ax1.set_ylabel('[m], [m/s], [m/s2]')
    fig1.tight_layout()
    fig1.savefig('esys_mck_earthquake_v1.1.png')

