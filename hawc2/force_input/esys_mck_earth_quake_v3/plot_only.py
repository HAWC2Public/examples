# -*- coding: utf-8 -*-
"""
Created on Tue May 31 14:18:23 2016

@author: dave
"""

import os
from matplotlib import pyplot as plt
from wetb.prepost import windIO


def read_hawc2(fname):
    p1 = os.path.dirname(fname)
    p2 = os.path.basename(fname)

    try:
        res = windIO.LoadResults(p1, p2)
    except Exception as e:
        print('failed loading:', fname)
        print(e)
        return

    df = res.ch_df.copy()
    df['chi'] = df.index
    df.set_index('unique_ch_name', inplace=True)
    return res.sig2df(), df


if __name__ == '__main__':

    fig1, ax1 = plt.subplots(figsize=(12,3))
    fname = 'res/esys_mck_earthquake_v1.1'
    df, ch_df = read_hawc2(fname)
    time = df['Time']

    ch = 'global-tower-elem-001-zrel-0.00-State acc-x'
    ax1.plot(time, df[ch], 'k-', label='acc x')

    ch = 'global-tower-elem-001-zrel-0.00-State vel-x'
    ax1.plot(time, df[ch], 'r--', label='vel x')

    ch = 'global-tower-elem-001-zrel-0.00-State pos-x'
    ax1.plot(time, df[ch], 'y-.', label='pos x')

    ax1.grid()
    ax1.legend(loc='best')
    ax1.set_title('tower base pos, vel and acc')
    ax1.set_xlabel('time')
    ax1.set_ylabel('[m], [m/s], [m/s2]')
    fig1.tight_layout()
    fig1.savefig('esys_mck_earthquake_v1.1.png')
