import numpy as np


def balance_timeseries(EQ_file, skiprows=0, save_path=False):
    """This function can be used to shift acceleration time series
    in order for them to integrate to 0.

    Parameters
    ----------
    EQ_file : str
        Path to file containing the acceleration time series
        
    skiprows : int
        Number of lines to skip in the input file.
    
    save_path : str
        Path to save the balanced time series to.
    """
    EQ_accelerations = np.loadtxt(EQ_file, skiprows=skiprows)

    balanced_accelerations = EQ_accelerations.copy()
    format = ['%.3f']
    for i in range(1, EQ_accelerations.shape[1]):
        balanced_accelerations[:, i] = EQ_accelerations[:, i] - np.trapz(EQ_accelerations[:, i], EQ_accelerations[:, 0]) / np.ptp(EQ_accelerations[:, 0])
        format.append('%.6f')
    
    if save_path:
        with open(EQ_file) as f:
            header="\n".join([s.strip() for s in f.readlines()[:skiprows]])
        np.savetxt(
            fname=save_path,
            X=balanced_accelerations,
            fmt=format,
            delimiter="   ",
            header=header,
        )
    return balanced_accelerations


balance_timeseries("earthquake_acc_field.qda", skiprows=20, save_path="./earthquake_acc_field_balanced.qda")
