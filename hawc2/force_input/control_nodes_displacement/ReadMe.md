# Control nodes displacement

## Introduction

This example shows how to control the displacement of 2 main body nodes.

## Modelling

The model is composed by:

  -	A dummy, rigid, body fixed to ground.
  -	The blade.
  - An MCK external system with an identity matrix as mass matrix. It has 3 states to control the displacement of the first node, and another 3 for the second.
  -	The blade root rotation is fixed to 0.
  -	The displacement of the blade root and the node at 2/3 span is linked to the state of the external system, through `cstr_meas_relative_disp_body2body`.
  - The displacement of the external system is read from a file, through `cstr_esys_dof_from_file`.

There is also a python script to generate 2 simple displacement fields. The displacement field must have the same time step as the simulation.

The output of the MCK external system is also written out. The outputs are ordered as: displacement for all states, velocity for all states, acceleration for all states, residual for all states.


## Running the example

The examples can be run using the following command line:

```cmd
HAWC2MB.exe htc\model_forced_displacement_2_nodes.htc
```
