# -*- coding: utf-8 -*-
"""
Write a basic displacement field, starting from the acceleration.

@author: ricriv
"""

import numpy as np
from scipy.integrate import cumulative_trapezoid
import matplotlib.pyplot as plt

# Time step. Must be the same as the simulation.
dt = 0.01

# Time array.
time = np.arange(0.0, 20.0, dt)

# Acceleration field.
# First 3 columns for node 1. Next 3 columns for node 2.
acc = np.zeros((time.size, 6))

# Ramp and constant.
# acc[:, 0] = np.minimum(time, 1.0)
# acc[:, 3] = acc[:, 0]

# Sinusoidal.
acc[:, 0] = np.sin(2.0*np.pi*0.5*time)
acc[:, 3] = 0.8 * acc[:, 0]

# Integrate acceleration to get velocity and displacement.
vel = cumulative_trapezoid(acc, dx=dt, axis=0, initial=0.0)
disp = cumulative_trapezoid(vel, dx=dt, axis=0, initial=0.0)

# Save displacement.
np.savetxt("./data/support_displacement.txt", disp)

# Plot.
fig, ax = plt.subplots()
ax.set_xlabel("Time [s]")
ax.set_ylabel("Displacement [m]")
ax.grid()
ax.plot(time, disp[:, 0], label="Support 1")
ax.plot(time, disp[:, 3], label="Support 2")
ax.legend()
