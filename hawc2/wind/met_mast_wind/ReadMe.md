# Met-mast wind input

## Introduction

This example illustrates how the `met_mast_wind` command can be used to supply a wind-input to a HAWC2 simulation. The model can be run using HAWC2 on any platform.

## Modelling

The `met_mast_wind` command models the wind field along a 1-dimensional line in the z coordinate. The wind components (u,v,w) can be specified at multiple levels, providing both a shear and veer profile varying it with time.

## Specifying the time series

The time series of the wind speeds at the met-mast are given as space-separated-values with the u, (v and w) component(s) organized in columns. The data file can contain both one two or three wind speed components. To define the wind components at multiple levels/heights, multiple sets of u, (v and w) columns can be defined. An example of a `met_mast_wind` input file containing u and v components at 2 levels is shown below. Any descriptive headers can be added at the top of the input file be prefacing them with a hash `#`.

```St
#         level 1                   level 2
#    u              v           u            v       
0.000000e+00 0.000000e+00 0.000000e+00 0.000000e+00
2.512562e-02 0.000000e+00 2.512562e-02 0.000000e+00
5.025125e-02 0.000000e+00 5.025125e-02 0.000000e+00
7.537688e-02 0.000000e+00 7.537688e-02 0.000000e+00 
```

## Reading the time series in HAWC2

The file containing the met-mast time series is declared in the `wind` block with the command `met_mast_wind`. The details of the file are then provided in the `met_mast_wind` sub-block in the `wind` block. Here the time step, start time, number of components contained in the file, and the number of levels in the file are specified.

```HAWC2
  begin met_mast_wind ;
    deltat     0.5 ;
    t0         1.0 ; time from which the wind field start
    components 2   ; (1=u, 2=u,v, 3=u,v,w)
    nlevels    4   ;
    level 1    0.0 ;
    level 2  -50.0 ;
    level 3 -100.0 ;
    level 4 -150.0 ;
  end met_mast_wind;
```

## How HAWC2 calculates wind speed

The `met_mast_wind` command can be used along with multiple other wind inputs. The calculation of the windspeed in HAWC2 is done according to the following formula:

```HAWC2
wsp = action_windspeed_u + gust
+ (wsp_mean*wind_ramp_factor+wind_ramp_abs)*shear_factor
+ (wsp_mean*wind_ramp_factor+wind_ramp_abs)*user_defined_shear
+ met_mast_wind + dwm_deficit_u*wind_ramp_factor
+ dwm_turb*wind_ramp_factor
+ turb * scaling * user_defined_shear_turbulence * wind_ramp_factor
+ user_wind_dll velocity
```

## Writing met-mast time series
Met-mast time series can be created using the function `write_met_mast_wind_data()` in the python script `create_met_mast_wind.py`.

## Running the example
The examples can be run using the following command line:

Example using the NREL 5MW Reference turbine with an unbalanced acceleration input:

```cmd
HAWC2MB.exe met_mast_wind.htc
```

## Notes

Please beware that the HAWC2 input reader will not throw an error on incorrectly formatted input files. If the input file reader comes across an invalid line, the windspeed at that time will be replaced with a 0.
