#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 23 15:53:46 2019

@author: dave
"""

import numpy as np


def write_met_mast_wind_data(fname):
    """
    """
    # needs to be aligned with the met_mast_wind block in the htc file
    components = 2 # 1 for u only, 2 for u,v, and 3 for u,v,w

    # 100 seconds simulation using a deltat of 0.5 in met_mast_wind and needs
    # to be consistant with what is in the htc file in the met_mast_wind block
    nr_dx = int(100/0.5) # number of time steps, H2 meteo u-dir

    # vertical, H2 meteo w-dir, needs to be consistant with the
    # nlevels as defined in the htc file in the met_met_wind block
    nlevels = 4

    headers = {1:['u'],
               2:['u', 'v'],
               3:['u', 'v', 'w'],
               }

    nr_cols = nlevels*components

    wind = np.zeros((nr_dx, nlevels*components))

    # Example: include a mean wind speed change in the met_mast data
    u_change = np.linspace(0, 5, nr_dx)#.reshape((nr_dx,1))
    for i in range(0, nr_cols, components):
        wind[:,i] = u_change

#    # Example: add a shear profile that changes over time
#    for i in range(0, nr_dz*input_type, 2):
#        wind[:,i] += (i*2 + 1)

#    # add a veer profile that changes over time
#    for i in range(1, nr_dz*input_type, 2):
#        wind[:,i] += 2 - i

    # column width of the text file
    cw = 25 # update when you would change fmt in np.savetxt
    cw_levels = cw*components
    level_header = ''.join([f'level {k+1}'.center(cw_levels) for k in range(nlevels)])
    header = ''.join([k.center(cw) for k in headers[components]*nlevels])
    with open(fname, 'w') as fio:
        fio.writelines(['#'+level_header[2:]+'\n', '#'+header[2:]+'\n'])
        np.savetxt(fio, wind, fmt='%.18e') # results in cw of 25


if __name__ == '__main__':

    write_met_mast_wind_data('met_mast_wind.txt')

